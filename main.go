package main

import (
	"encoding/json"
	"flag"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/Niesch/JuliaCore"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"syscall"
)

type JuDiGo struct {
	DiscordToken string          `json:"discord_token"`
	Prefix       string          `json:"prefix"`
	Julia        juliacore.Julia `json:"juliacore"`
}

func main() {
	conf := flag.String("conf", "conf.json", "JuDiGo configuration file")
	flag.Parse()

	c, err := ioutil.ReadFile(*conf)
	if err != nil {
		log.Fatal(err)
	}
	var jdg JuDiGo
	err = json.Unmarshal(c, &jdg)
	if err != nil {
		log.Fatal(err)
	}

	var julia *juliacore.Julia
	julia = &jdg.Julia
	dg, err := discordgo.New("Bot " + jdg.DiscordToken)
	if err != nil {
		log.Fatal(err)
	}

	// Register the handleMessageCreate func as a callback for MessageCreate events.
	dg.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		handleMessageCreate(s, m, julia, jdg.Prefix)
	})

	err = dg.Open()
	if err != nil {
		log.Fatal(err)
	}

	// Wait for signals
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}
