package main

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/Niesch/JuliaCore"
	"strings"
)

func handleMessageCreate(s *discordgo.Session, m *discordgo.MessageCreate, j *juliacore.Julia, prefix string) {
	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	// Prefix required, but string must not be only prefixes
	if strings.HasPrefix(m.Content, prefix) && strings.Replace(m.Content, prefix, "", -1) != "" {
		line := strings.TrimPrefix(m.Content, prefix)
		if idx := strings.IndexByte(line, ' '); idx >= 0 {
			resp, err := j.RespondTo(line[:idx], strings.TrimPrefix(line[idx:], " "))
			if err != nil {
				if err == juliacore.ErrNoMatch {
					return
				}
				s.ChannelMessageSend(m.ChannelID, err.Error())
				return
			}
			s.ChannelMessageSend(m.ChannelID, resp.Message)
		} else {
			resp, err := j.RespondTo(line, "")
			if err != nil {
				if err == juliacore.ErrNoMatch {
					return
				}
				s.ChannelMessageSend(m.ChannelID, err.Error())
				return
			}
			s.ChannelMessageSend(m.ChannelID, resp.Message)
		}
		return
	}
}
